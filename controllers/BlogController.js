const Article = require('../models/Model');


// récupérer tous les articles
const blog_index = (req, res) => {
    Article.find()
        .then((result) => {
            res.render('home', { title: 'Home blog', dataBlog: result });
        })
        .catch((err) => {
            res.redirect('/404');
        })
};

// créer un article
const blog_create = (req, res) => {
    res.render('create', { title: 'Créer un article' });
};


// à propos
const blog_about = (req, res) => {
    res.render('propos', { title: 'A Propos de nous' });
};

// contact
const blog_contact = (req, res) => {
    res.render('contact', { title: 'Nous Contacter' });
}

// ajouter un article
const blog_create_post = (req, res) => {
    const article = new Article(req.body)
    article.save()
        .then((result) => {
            res.redirect('/blog/article/' + result._id);
        })
        .catch((err) => {
            res.redirect('/404');
        })
}

// détails d'un article
const blog_details = (req, res) => {
    const id = req.params.id;
    Article.findById(id)
        .then((result) => {
            res.render('article', { title: 'Détails de article', article: result });
        })
        .catch((err) => {
            res.redirect('/404');
        })
};

// modifier un article
const blog_edit = (req, res) => {
    const id = req.params.id;
    Article.findById(id)
        .then((result) => {
            res.render('edit', { title: 'Modifier un article', article: result });
        })
        .catch((err) => {
            res.redirect('/404');
        })
};

// Valider la modification d'un article
const blog_edit_put = (req, res) => {
    const id = req.params.id;
    Article.findByIdAndUpdate(id, req.body)
        .then((result) => {
            res.redirect('/blog/article/' + id);
        })
        .catch((err) => {
            res.redirect('/404');
        })
};

// supprimer un article
const blog_delete = (req, res) => { 
    const id = req.params.id;
    Article.findByIdAndDelete(id)
        .then((result) => {
            res.redirect('/blog');
        })
        .catch((err) => {
            res.redirect('/404');
        });
};


module.exports = {
    blog_index,
    blog_create,
    blog_about,
    blog_contact,
    blog_create_post,
    blog_details,
    blog_edit,
    blog_edit_put,
    blog_delete
};