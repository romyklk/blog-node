const express = require('express');
const app = express();
const path = require('path');
const mongoose = require('mongoose');
const BlogRouters = require('./routers/BlogRouters');
require('dotenv').config()


const methodOverride = require('method-override'); // pour les méthodes PUT et DELETE


///port d'écoute
const port = process.env.PORT || 2020;

// Connexion à la base de données
mongoose.connect(process.env.DB_URL)
    .then((result) => app.listen(port))
    .catch((err) => console.log(err));


// Les middlewares
app.use(express.static('public')); // pour les fichiers statiques (css, js, images)
app.use(express.urlencoded({ extended: true })); // pour les formulaires (POST)
app.use(methodOverride('_method')); // pour les méthodes PUT et DELETE

app.get('/', (req, res) => {
    res.redirect('/blog');
});

app.use('/blog', BlogRouters);

app.set('view engine', 'ejs');
//app.set('views', path.join(__dirname, 'views'));

//let filePath = path.join(__dirname, 'views');





app.use((req, res) => {
    //res.sendFile(filePath + '/404.html');
    res.render('404', { title: '404 Not Found' });
});

