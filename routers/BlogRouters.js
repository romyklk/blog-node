const express = require('express');
const router = express.Router();
const Article = require('../models/Model');

const BlogController = require('../controllers/BlogController');

// Route GET pour la page d'accueil
router.get('', BlogController.blog_index);

// Route GET pour la page de création d'un article
router.get('/create', BlogController.blog_create);

// Route GET pour la page à propos
router.get('/propos', BlogController.blog_about);

// Route GET pour la page contact
router.get('/contact', BlogController.blog_contact);

// Route POST pour ajouter un article
router.post('/create', BlogController.blog_create_post);


// Route GET pour la page de détails d'un article
router.get('/article/:id', BlogController.blog_details);

// Route GET pour la page de modification d'un article
router.get('/article/:id/edit', BlogController.blog_edit);

// Route PUT pour modifier un article
router.put('/article/:id/edit', BlogController.blog_edit_put);

// Route DELETE pour supprimer un article
router.delete('/article/:id', BlogController.blog_delete);


module.exports = router;