# TEMPLATE ENGINE

## Description

Dans ce projet, nous allons voir comment utiliser un moteur de template pour générer des pages HTML dynamiquement.
Pour cela nous allons utiliser le moteur de template ejs (embedded javascript) qui est un moteur de template simple et efficace.
Le but de ce projet est de générer une page HTML qui affiche une liste d'articles de blog.

## Instructions

1. Clonez le repository
2. Installez les dépendances du projet avec la commande `npm install`
3. Lancez le serveur avec la commande  `node app.js`