const mongoose = require('mongoose');
const { v4: uuidv4 } = require('uuid');

const Schema = mongoose.Schema;

const articleSchema = new Schema({
    Id: {
        type: String,
        required: true,
        default: uuidv4()
    },
    titre: {
        type: String,
        required: true
    },
    auteur: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    resume: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
}, { timestamps: true });

//Création du modèle des articles
const Article = mongoose.model('Article', articleSchema);

module.exports = Article;



